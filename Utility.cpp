#include "Utility.h"

#include <iostream>

void Utility::PrintArr(const std::vector<std::string>& Words)
{
    using namespace std;

    const size_t Size = Words.size();
    string Str;
    
    for (size_t Idx = 0; Idx < Size; Idx++)
    { 
        if(Idx != 0)
        {
           Str += ", ";
        }
        Str += Words[Idx];
    }
    cout << "(" + Str + ")" << endl;
}
